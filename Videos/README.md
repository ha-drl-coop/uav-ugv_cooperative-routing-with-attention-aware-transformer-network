1.  The folders contain animations of the UAV-UGV cooperative routes for sample scenarios from each problem size and task point distribution. We show the routes obtained from the Guided Local Search heuristics and the Deep Reinforcement Learning policy with a sampling decoding strategy. 

2.  Sample scenarios from each problem size and task point distribution are also shown. 

[Watch the video on YouTube](https://www.youtube.com/watch?v=94NyNQ414kg)

<a href="https://www.youtube.com/watch?v=94NyNQ414kg">
    <img src="https://img.youtube.com/vi/94NyNQ414kg/0.jpg" alt="Watch the video" style="width:1080px;height:720px;">
</a>